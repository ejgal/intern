# Dokumentasjon for medlemmer av radiOrakel

Dokumentasjonen er skrevet i reStructured Text og kompileres av sphinx.
Ved commit til master oppdateres intern.radiorakel.no automatisk.

[Sphinx style guide](https://documentation-style-guide-sphinx.readthedocs.io/en/latest/style-guide.html)
