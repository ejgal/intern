Formater og merking av lydfiler
===============================

Vårt oppsett godtar lydfiler i formatene flac (.FLAC), mp3 (.MP3) og Ogg Vorbis (.OGG). Andre formater må konverteres til flac, ogg eller mp3 før opplasting til Airtime. Alt som skal lastes opp i Airtime må merkes riktig, slik at det blir enkelt å finne dem igjen senere. Feilmerkede filer ’forsvinner’!

Hva er merking?
---------------


Merking / tagging er *metadata*, informasjon om lydfilen. Det er denne informasjonen som gjør at telefon / PC / etc kan vise navn på låt og artist når låta spilles av.



Standard for merking
--------------------

Felter i **bold** er obligatoriske.

+-------------+-------------------+----------------+----------------------------+----------+---------------------------+
| Filtype     | Title             | artist         | Album                      | Date     | Genre                     |
+=============+===================+================+============================+==========+===========================+
| Program     | Beskriv innholdet | **radiOrakel** | **PROGRAMNAVN ÅÅÅÅ-MM-DD** | **ÅÅÅÅ** | **Program**               |
+-------------+-------------------+----------------+----------------------------+----------+---------------------------+
| Spilleliste | **Låt-tittel**    | **Artistnavn** | Album                      |          | **Spilleliste # ÅÅÅÅ-MM** |
+-------------+-------------------+----------------+----------------------------+----------+---------------------------+
| Jingle      | **Beskrivelse**   | **radiOrakel** |                            | **ÅÅÅÅ** | **Jingle**                |
+-------------+-------------------+----------------+----------------------------+----------+---------------------------+
| Spot        | **Beskrivelse**   | **radiOrakel** |                            | **ÅÅÅÅ** | **Spot**                  |
+-------------+-------------------+----------------+----------------------------+----------+---------------------------+
| Serie       | **Episode ##**    | **radiOrakel** | **Serienavn**              | **ÅÅÅÅ** | **Serie**                 |
+-------------+-------------------+----------------+----------------------------+----------+---------------------------+

Evnt kan vi ha det på listeform, da blir det lettere å vise eksempler. Og det er
plass til å vise på alle enheter.

**Program**

* Title - Beskrivelse av innhold
* **Artist** - radiOrakel
* **Album** - Programnavn ÅÅÅÅ-MM-DD (F.eks Horisonten 2019-01-18)
* **Date** - ÅÅÅÅ (F.eks 2019)
* **Genre** - Program

**Spilleliste**

* **Title** - Låt-tittel
* **Artist** - Artistnavn
* Album - Albumnavn
* **Genre** - Spilleliste # ÅÅÅÅ-MM

**Jingle**

* **Title** - Beskrivelse
* **Artist** - radiOrakel
* **Date** - ÅÅÅÅ
* **Genre** - Jingle


Merking av programmer
---------------------

**Obligatorisk**

* Artist: radiOrakel
* Album: Programnavn ÅÅÅÅ-MM-DD (F.eks Horisonten 2019-01-18)
* Date: ÅÅÅÅ (F.eks 2019)
* Genre: Program

**Valgfritt**

* Title: Beskrivelse av innholdet, navn på programleder / tekniker / gjest o.l.


Merking av spillelistefiler
---------------------------
Musikkfiler skal beholde sin opprinnelige merking, men filer til playlist må i tillegg ha

* Genre: Spilleliste # ÅÅÅÅ-MM (F.eks Spilleliste #1 2019-01)


Hvordan merke lydfiler
----------------------

Merking gjøres ved hjelp av egne programmer. Når du ripper en CD vil du som regel minst ha mulighet til å merke artist, tittel, album og år, men mange av programmene har en del begrensninger, enten på antall felter eller på format. Vi anbefaler å bruke MusicBrainz Picard.


MusicBrainz Picard
------------------


Standarden for metadata til musikkfiler heter id3 og dessverre finnes det flere versjoner av standarden. For at særnorske tegn som æ, ø og å (eller andre sære tegn som feks ł, û, ƈ , etc) skal vises riktig og være søkbare i Airtime må vi bruke id3v2.4 med tegnsett UTF-8.



.. figure:: picard_id3.png

    I Picard, velg ’Options > Options > Tags’ og herm etter valgene til venstre.

Picard er installert på radioens maskiner og stilt inn så standarden id3v2.4 brukes automatisk. Hvis du jobber på andre maskiner / med andre programmer, sjekk innstillinger for å se at du bruker riktig format. Picard kan lastes ned `her <https://picard.musicbrainz.org/>`_


Når Picard er riktig innstilt er det på tide å merke noen filer!



.. figure::  picard_add.png

  Trykk ’Add Files’ eller ’Add Folder’ og velg filen(e) som skal merkes. Filen(e) legger seg under ’Unmatched Files’


.. figure:: picard_tag.png

    Marker filen du vil merke. Nederst i vinduet ser du diverse info om fila. Under ’New Value’ skriver du inn riktige verdier i feltene.


.. figure:: picard_missing_tag.png

    Hvis en tag som feks ’Genre’ mangler, høyreklikk i et av feltene under ’Tag’ og velg ’Add New Tag...’ I dialogen som kommer opp, skriv ’g’ og velg genre i det øverste feltet. Trykk ’Edit value’ og fyll inn.

Når fila er ferdig merket, trykk på ’Save’! Som en test kan du åpne filen i en mediespiller og se at metadataen er oppdatert.

Filformater
-----------

Det vanligste formatet for ’rå’ lyd er Wave / PCM, med etternavn .wav. Dessverre fungerer ikke airtime pålitelig med wave-filer, i tillegg tar de mye diskplass. Vår installasjon er testet og fungerer bra med formatene formatene **FLAC**, **OGG** og **MP3**.

**FLAC** er komprimerte filer (de tar ca halvparten så stor plass som wave) men uten tap av informasjon. Det betyr at de er egnet til eventuell senere redigering og mye bedre til lagring enn .wav. Derfor anbefaler vi **FLAC** til programmer, serier, etc – alt egenprodusert materiale det kan tenkes behov for å redigere igjen på et senere tidspunkt.

**MP3** og **OGG** er komprimert med tap av informasjon. Informasjonen som tas vekk er stort sett ikke hørbar og filer i god kvalitet har ingen merkbar forskjell fra .wav eller **FLAC** ved avspilling. Men: de er IKKE egnet til videre redigering, da må man regne med hørbart tap av kvalitet.

**MP3** og **OGG** anbefales derfor til lyd som ikke skal bearbeides videre, musikk, spot’er, jingler, etc.


.. table:: Resultat av konvertering mellom noen formater

    ===========  =================  ===============================
    Format inn   Konvertert format  Resultat
    ===========  =================  ===============================
    .WAV          .FLAC              lydinformasjon bevares
    .FLAC         .WAV               lydinformasjon bevares
    .WAV / FLAC   MP3, OGG           Ikke hørbar informasjon fjernes
    .MP3          .OGG               Hørbart kvalitetstap
    .OGG          .MP3               Hørbart kvalitetstap
    ===========  =================  ===============================

Konvertering
------------

Freac http://www.freac.org/ er et konverteringsprogram som kan kjøre på Windows, Mac og Linux.

.. figure:: freac_add.png

    Velg File > Add > Audio File(s) eller Audio CD contents. Eller dra filen inn i programmet. Du kan konvertere mange filer samtidig.inter

.. figure:: freac_folder.png

    Filen(e) du konverterer havner i mappen markert i Output folder. På pcene våre i en mappe på skrivebordet som heter ripp.

.. figure:: freac_convert.png

    Velg Encode > Start Encoding with > 'formatet du ønsker'.
