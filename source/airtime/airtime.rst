Bruk av airtime
===============

Du logger inn i airtime på `https://airtime.radiorakel.no/ <https://airtime.radiorakel.no/>`_ med tildelt brukernavn og passord.


Bruk Slack!
-----------

Alle brukere av Airtime må venne seg til å bruke `Slack <http://slack.radiorakel.no/>`_ eller app (finnes til Android, iPhone og Windows Mobile). Slack er et internt område for chat, meldinger og spørsmål / svar.

Det er feks her det vil informeres om planlagt nedetid for server, derfor viktig at man ’titter innom’ før man laster opp noe til Airtime.


Brukernivåer
---------------------

Airtime har 4 brukernivåer:

* **Guest**: kan se hva som spiller når. Tekniker-PC har en gjestebruker.
* **DJ**: som guest + redigere innhold i tildelte programmer og laste opp og slette egne lyd-filer
* **Program Manager (PM)** – som DJ + kan redigere innhold i alle programmer og håndtere alle lyd-filer
* **Admin** – som PM + justere innstillinger og legge til eller fjerne brukere

Musikkredaktør, redaktør og teknisk sjef har brukere med nivå PM. Hver redaksjon får tildelt en bruker med nivå DJ.


Laste opp lydfiler
------------------

Husk at Airtime er testet med formatene beskrevet under Filformater /konvertering. Husk også at filene må merkes skikkelig som beskrevet under Hvordan merke lydfiler.

Alle aktive jingler og spot’er er allerede lagt inn og ikke nødvendig å laste opp.

.. figure:: airtime_add.png

   Klikk på ’Add Media’ for å komme til opplasting.

.. figure:: airtime_upload.png

   Legg filene i opplastingsområdet og klikk ’Start upload’. Ha vinduet åpent til opplastingen er ferdig.

.. figure:: airtime_library.png

   Klikk på ’Library’ for å sjekke at filene dine er lastet opp og rett merket. Velg hvordan filene i biblioteket sorteres ved å klikke på de små pilene i kolonnene ’Title’, ’Creator’, osv. Her er filene sortert etter ’Uploaded’ slik at nye filer vises øverst.


.. figure:: airtime_metadata_select.png

   Hvis merkingen på en enkelt fil er feil, klikk den og velg ’Edit Metadata’.

.. figure:: airtime_metadata_edit.png

   Her kan du fylle inn riktig metadata og trykke Save når du er ferdig.


Fylle programpost
-----------------

Klikk på ’Calendar’ for å få opp oversikten over programmerte sendinger.

.. figure:: airtime_program_select.png

   Finn ditt program, klikk på det og velg ’Add / Remove Content’

.. figure:: airtime_program_fill.png

   Det kommer opp et vindu med to bokser: biblioteket på venstre side, valgt program på høyre. Markér filene du vil flytte og trykk ’Add to selected show’ eller klikk og dra filene på plass. Trykk ’OK’ når du er ferdig. Airtime spiller da av lyden din til ønsket tid.


Smart block og playlist
-----------------------

For aktive og avanserte brukere. Undersøk og lær selv!
