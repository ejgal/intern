Opptak
======


PC på teknikerrommet er satt opp med PlayIt Recorder som kan ta opp sendinger manuelt og lagre dem som wave-filer. Dette formatet er valgt for at det skal være mulig å redigere opptakene etter sending uten tap av lydkvalitet.



.. figure:: opptak_hvor.png

    Start PlayIt ved å klikke på den røde sirkelen i verktøylinjen nederst til høyre på skjermen.


.. figure:: opptak_oversikt.png

   For å starte opptak, trykk på den store røde sirkelen.

.. figure:: opptak_navn.png

   For å starte ett opptak må du gi den et navn. Hvis opptaket er et program som skal legges inn i airtime skriv programnavnet, uten mellomrom, bruk eventuelt bindestrek. Ikke legg til dato, det legges automatisk til.

.. figure:: opptak_finn.png

   For å finne opptaket, klikk Recordings > Explore Recordings


Når du er ferdig, trykk stopp (den firkantede knappen).

Ofte vil det være nødvendig å trimme start og / eller slutt på programmet. Det kan da åpnes i feks Hindenburg (et redigeringsprogram installert på radioens stasjonære PC’r).
