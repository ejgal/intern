################
Tonorapportering
################

For at pengene vi betaler til Tono skal gå til artistene vi faktisk spiller må
vi rapportere musikkbruk.


*******
Oppsett
*******

Bruk `denne malen
<https://docs.google.com/spreadsheets/d/1hcgw1gvLYeA5umlsw5QLptvWtZr4lgLr9j1xQY-Yr9M/edit?usp=sharing>`_.
Hvis du har en google-konto kan du trykke **File -> Make a copy** for å fylle ut
online. På den måten kan dere med større redaksjoner samarbeide om et dokument.

Hvis du heller vil laste ned malen og redigere i libreoffice eller excel, gå til
**File -> Download as** og velg .ods eller .xslx. Legg også merke til
alternativet Comma-separated values, det er formatet filen skal leveres i.

.. figure:: download_as.png

   Last ned malen i forskjellige formater.


*********
Utfylling
*********

Når vi mottar rapportene slår vi de sammen med avspillingslogger for airtime og
lager en stor fil. For at det skal gå greit er det viktig at rapportene er
formatert likt.

* Title - Tittel på låt.
* Creator - Låtartist
* Length - Lengde på låt i formatet **MM:SS**.
* Antall_ganger - Hvor mange ganger låten er spilt i rapporteringsmåneden. Husk repriser!

Title, creator og antall ganger er ganske selvforklarende. Feltet Length er det
som oftest blir feil. Malen er satt opp sånn at man skriver inn lengden på
formatet 0:MM:SS, og så vises det som MM:SS. Det er viktig at formatet er
**MM:SS** og ikke bare M:SS, selv om låten er mindre enn 10 minutter.




********
Levering
********

Ferdige utfylte rapporter konverteres til formatet comma-separated values
(.csv).

* Google sheets - **File -> Download AS -> Comma-separated values**
* Libreoffice, excel osv - **File -> Save As**, velg .csv.

Kall filen REDAKSJON_MÅNED.csv og send til redaktor@radiorakel.no med emne Tono
REDAKSJON MÅNED.
