
===========================
Oversikt over dokumentasjon
===========================

Her finner du all dokumentasjon som er skrevet for medlemmer av RadiOrakel.
Hvis du er ny kan du starte med å lese :ref:`velkomstbrevet <velkommen>`.

.. toctree::
   :maxdepth: 1
   :caption: Diverse

   diverse/velkommen
   diverse/lenker
   diverse/ordliste

.. toctree::
   :numbered:
   :maxdepth: 1
   :caption: Airtime

   airtime/intro
   airtime/formater_og_merking
   airtime/opptak
   airtime/airtime

.. toctree::
   :numbered:
   :maxdepth: 1
   :caption: RadiOrakel.no

   nettsiden/bruker
   nettsiden/publisere
   nettsiden/programside
   nettsiden/podkast


.. toctree::
   :numbered:
   :maxdepth: 1
   :caption: Redaksjoner

   redaksjoner/tonorapport


.. toctree::
   :numbered:
   :maxdepth: 1
   :caption: Vedtekter

   vedtekter/vedtekter
