import json


source_suffix=['.rst']

extensions = ['sphinx.ext.mathjax']


html_title = "Dokumentasjon for RadiOrakel"
html_theme = "sphinx_rtd_theme"
master_doc = 'index'
project = u'RadiOrakel dokumentasjon'
copyright = u'2019, RadiOrakel'

version = ''
release = ''
