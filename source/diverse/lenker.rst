
==============
Nyttige lenker
==============

-----
Promo
-----

RadiOrakels sosiale medier, nettside og patreon-konto. Spre ivei!

* `radiorakel.no/kurs <https://radiorakel.no/kurs>`_
* `radiorakel.no/bidra <https://radiorakel.no/bidra>`_
* `patreon.com/radiorakel <https://patreon.com/radiorakel>`_
* `facebook.com/radiorakel <https://facebook.com/radiorakel/>`_
* `twitter.com/radiorakel <https://twitter.com/radiorakel>`_
* `instagram.com/radiorakel <https://www.instagram.com/radiorakel/>`_
* `radiorakel.no/livestream <https://radiorakel.no/livestream>`_


---------------
Interne verktøy
---------------

* `slack.radiorakel.no <https://slack.radiorakel.no>`_ - Internkommunikasjon
* `reservasjon.radiorakel.no <https://reservasjon.radiorakel.no>`_ - Reservere rom og utstyr
* `airtime.radiorakel.no <https://airtime.radiorakel.no/>`_ - Avviklingsverktøy
*  `radiorakel.no/login <https://radiorakel.no/login>`_ - Publisere på nettsiden
* `webmail.radiorakel.no <https://webmail.radiorakel.no>`_ - Epost (verv og redaksjoner)


-------
Verktøy
-------

Verktøy som er installert på radioens PCer.

* `Hindenburg journalist <https://hindenburg.com/products/hindenburg-journalist>`_ - Lydredigering
* `Audacity <https://www.audacityteam.org/>`_ - Lydredigering
* `Freac <https://www.freac.org/>`_ - CD-ripping og filkonvertering
* `Musicbrainz picard <https://musicbrainz.org/>`_ - Merking av lydfiler
* `Firefox <https://www.mozilla.org/en-US/firefox/new/>`_ - Nettleser
* `Infrarecorder <http://infrarecorder.org/>`_ - Brenne CDer
* `VLC media player <https://www.videolan.org/vlc/index.html>`_ - Mediespiller
* `Notepad++ <https://notepad-plus-plus.org/>`_ - Enkle notater
* `Libreoffice <https://www.libreoffice.org/>`_ - Tekstformatering, regneark	++
* `Sumatra <https://www.sumatrapdfreader.org/free-pdf-reader.html>`_ - PDF-leser
* `7zip <https://www.7-zip.org/>`_ - Komprimering


---------------
Eksterne guider
---------------

* `Hindenburg journalist <https://hindenburg.com/support/guides?pid=1128>`_
