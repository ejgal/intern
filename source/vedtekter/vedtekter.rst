

Stadfestet på ekstraordinært årsmøte 13. august 2018.

.. _vedtekter:

======
Formål
======

---------------------
Feminisme, integritet
---------------------

RadiOrakel er Oslos feministiske lokalradio. RadiOrakel søker å nå og engasjere alle
lyttere i Oslo-området. RadiOrakel skal drives på en slik måte at det aldri kan
stilles spørsmålstegn ved RadiOrakels journalistiske integritet.


-----------------------
Bekjempe diskriminering
-----------------------

RadiOrakel bekjemper all diskriminering og undertrykking på grunnlag av kjønn,
etnisk bakgrunn, seksualitet, livssyn eller klasse.

----------------------------------------
Fremme kvinner / minoritetskjønn i media
----------------------------------------

RadiOrakel arbeider for å bekjempe sexisme i medier generelt, og for å øke
andelen kvinner og minoritetskjønn i massemediene. RadiOrakel skal derfor ha en
redaktør som er kvinne eller minoritetskjønn.

RadiOrakel søker å engasjere og lære opp kvinner og minoritetskjønn på alle felt
innen radioarbeid og øke andelen kvinner og minoritetskjønn som intervjuobjekter
og deltakere i program.

Ledelsen på RadiOrakel består av redaktør, styreleder og redaksjonssjef. 2 av
disse skal være kvinner / minoritetskjønn og minimum 50% av styre og
redaksjonsråd skal være kvinner / minoritetskjønn. Medlemsmassen skal ha
:math:`\frac{2}{3}` kvinner / minoritetskjønn.

----------
Vær Varsom
----------

RadiOrakels mål er å lage radio av høy standard både innholdsmessig og teknisk,
og oppmuntre medlemmene til innsats for alternativ, eksperimentell og kritisk
journalistikk.

RadiOrakels journalistiske virksomhet skal være i tråd med RadiOrakels vedtekter
spesielt, og ellers følge retningslinjene i Vær Varsom-plakaten.

--------------
Selvstendighet
--------------

RadiOrakel er ikke talerør for bestemte grupper eller organisasjoner.
RadiOrakels radiovirksomhet skal alltid drives av aktive i RadiOrakel.
RadiOrakel skal ikke selge sendetid til organisasjoner eller virksomheter.

---------
Samarbeid
---------

RadiOrakel søker å utvikle samarbeid med grupper og organisasjoner som ikke
bryter med RadiOrakels formål f.eks. i form av konserter, seminarer, eksterne
prosjekter, kampanjer o.l.

----
Navn
----

Navnet ”RadiOrakel” følger dette formålet.


==========
Medlemskap
==========


---------------
Støttemedlemmer
---------------

Enhver som støtter formålet kan bli støttemedlem. Støttemedlem som har betalt kontingent for
inneværende år har møte- og talerett - men ikke stemmerett - på RadiOrakels medlems- og
årsmøter.

.. _medlemmer:

---------
Medlemmer
---------

Som medlem regnes alle som:

* støtter RadiOrakels formål
* har betalt kontingent for inneværende år
* har deltatt på RadiOrakels kurs eller tilsvarende opplæring

og som etter sist årsmøte har gjort minst en av følgende:

* vært aktiv i en redaksjon
* produsert innhold til programmer
* deltatt på minst 2 inntektsbringende arrangementer, dugnader eller lignende
* hatt et verv

Medlemmer har møte-, tale- og stemmerett på RadiOrakels medlems- og årsmøter.

==================================
Medlemmenes rettigheter og plikter
==================================

-----------
Rettigheter
-----------

Medlemmer som fyller kravene gitt i :ref:`Medlemskap -> Medlemmer <medlemmer>` kan

* delta i en eller flere redaksjoner
* være programleder / tekniker
* produsere innslag
* få pressekort / akkreditering
* anmode om at det avholdes medlemsmøte
* kreve en sak behandlet på medlemsmøte
* 5 medlemmer kan kreve at det avholdes medlemsmøte
* stille til valg

-------
Plikter
-------

Medlemmene forplikter seg til

* å delta på minst to medlemsmøter i året
* å holde seg orientert om vedtak som fattes på medlemsmøter og i andre fora på RadiOrakel
* å følge regler for booking av bærbart utstyr og studio, til å overholde innleveringsfrister og melde fra hvis noe er i stykker
* å vise disiplin og ta hensyn til hverandre. Programproduksjon og hensynet til radioens ansikt utad må gå foran private uoverensstemmelser
* å sørge for at radioen har oppdatert kontaktinformasjon

----------
Eksklusjon
----------

Medlemmer og støttemedlemmer som aktivt motarbeider andre medlemmer, radioens
formål eller på annet vis opptrer til skade for RadiOrakel kan ekskluderes.
Slike saker meldes Redaksjonsrådet som fatter vedtak.

Eksklusjonsvedtak kan ankes til medlemsmøte. Anken skal sendes styret som
innkaller til medlemsmøte og lager innstilling til vedtak. Ekskludert medlem er
suspendert fra sine verv og funksjoner i radioen til anken er behandlet.


===========
Redaksjoner
===========

Hver programpost skal ha en redaksjon. Redaksjonsmedlemmer forplikter seg til å
følge turnusordningen, til selv å sørge for rokeringer når det er nødvendig og
til å gi beskjed om permisjon i god tid.

Redaksjonene har et særlig ansvar for at programmene holder et tilfredsstillende
kvalitetsnivå. Ved tvil skal sending av program eller innslag utsettes i påvente
av vurdering i redaksjonsrådet, hos teknisk sjef eller hos redaktøren.

Alle redaksjoner plikter å komme med minimum ett forslag til inntektsbringende
prosjekt i året. Redaksjonen bør selv kunne gjennomføre prosjektet.

Medlemmene oppfordres generelt til å bidra til radioens drift, f.eks. gjennom
dugnadsarbeid, salg av støttelisenser, utdeling av PR-materiale osv.


============
Medlemsmøtet
============

Medlemsmøter avholdes minst 6 ganger i året, eller når minst 5 medlemmer krever
det skriftlig. Redaktør, redaksjonsråd eller styret innkaller til medlemsmøtet.

Medlemsmøtet skal behandle sakene som nevnes i innkallingen og ellers planlegge
og delegere ansvar for dugnader og andre aktiviteter til gagn for radioen.

Medlemsmøtet er vedtaksført når møtet er lovlig innkalt, dvs at innkalling med
saksliste er sendt ut med minst 14 dagers varsel. Saker som ønskes behandlet på
medlemsmøtet meldes styre eller redaksjonsråd.

Medlemsmøtet er ankeinstans for eksklusjonssaker.

Redaktør, styreleder, teknisk sjef og daglig leder har møteplikt på
medlemsmøtene og rapporterer fra sine verv. Redaksjonene informerer om sin
virksomhet.

Årets siste medlemsmøte oppnevner valgkomité for kommende årsmøte. Valgkomiteen
består av minst 3 personer og skal finne kandidater til alle verv som er på valg
på årsmøtet.


======
Styret
======

---------------------
Styrets sammensetning
---------------------

Styret velges av årsmøtet og består av styreleder, 4 styremedlemmer og 2
varamedlemmer. Funksjonstiden er ett år. Styreleder velges ved særskilt valg.

----------------
Styrets oppgaver
----------------

Styret gir retningslinjer for daglig drift og administrasjon, inngår avtaler av
vesentlig praktisk og økonomisk art og sørger for at disse blir overholdt.
Styret foretar ansettelser og oppsigelser og ivaretar personalsaker.

Styreleder har i fellesskap med daglig leder eller ansvarlig redaktør
forpliktende signatur for RadiOrakel, jf punkt 6.5 og 8.3.

Styret oppnevner forretningsfører / revisor etter behov og skal ellers lede
virksomheten i samsvar med lov, vedtekter og årsmøtets vedtak. Styret kan ta
alle avgjørelser som ikke i loven eller vedtektene er lagt til andre organer.


--------------
Styrets vedtak
--------------

Styret er vedtaksført når styreleder og minst 2 medlemmer er til stede. Vedtak
treffes med mer enn halvparten av de avgitte stemmene. Ved stemmelikhet avgjør
styreleders stemme.


----------
Styremøter
----------

Styreleder skal sørge for at styret holder møte så ofte som det trengs. Et
styremedlem, daglig leder eller redaktør kan kreve at styret sammenkalles.

Styret skal føre protokoll over styresakene.

Redaktør og teknisk sjef har møte- og talerett på styremøtene, men ikke
stemmerett.

------------
Daglig leder
------------

Styret utnevner daglig leder.

Daglig leder utfører oppgaver pålagt av styret og har i fellesskap med styreleder eller ansvarlig
redaktør forpliktende signatur for RadiOrakel, jf punkt 6.2 og 8.3.

Daglig leder rapporterer til styret og har møteplikt og talerett - men ikke stemmerett - på
styremøtene.


------------
Teknisk sjef
------------

Styret utnevner teknisk sjef.

Teknisk sjef har ansvar for ettersyn og vedlikehold av teknisk utstyr, samt å sette opp
teknikerturnus.

Teknisk sjef samarbeider med styret om planlegging og budsjettering av tekniske utgifter.

-------
IT-sjef
-------

Styret utnevner IT-sjef.

IT-sjef har ansvar for ettersyn og vedlikehold av IT-utstyr.

IT-sjef samarbeider med styret om planlegging og budsjettering av IT-utgifter.


===============
Redaksjonsrådet
===============

------------------------------
Redaksjonsrådets sammensetning
------------------------------

Redaksjonsrådet består av ansvarlig redaktør, redaksjonssjef, 4 medlemmer og 2
varamedlemmer. Funksjonstiden er ett år.

Redaksjonssjefen har ansvar for samarbeid og informasjonsflyt mellom
redaksjonene, og skal ellers bistå redaktøren etter behov. Redaksjonssjefen
utgjør sammen med styreleder og redaktør radioens ledelse.

Redaksjonsrådet velges på årsmøtet og skal ha en sammensetning som gir en god
representasjon av programproduksjonen i radioen.

Redaktør og redaksjonssjef velges ved særskilt valg.


-------------------------
Redaksjonsrådets oppgaver
-------------------------

#. Langsiktig, redaksjonell planlegging og styring
#. Oppfølging av formålsparagrafen, spesielt punkt 1.4 om kritisk, alternativ og eksperimentell journalistikk.
#. Fordele sendetid og turnusoppgaver mellom redaksjoner.
#. Vurdere medlemmenes status og ta opp nye medlemmer, jf punkt 7.5.
#. Brainstorming på nye programformer og programposter.
#. Intern skolering, jf punkt 7.6.
#. Vurdere programmer, jf punktene 4, 7.5 og 8.3.
#. Utpeke musikkredaktør.
#. PR: vise ansikt i samfunnsdebatten, slik som leserbrev, debattinnlegg osv.

-----------------------
Redaksjonsrådets vedtak
-----------------------

Redaksjonsrådet er vedtaksført når redaktør, redaksjonssjef og minst ett medlem er til stede.
Vedtak treffes med mer enn halvparten av de avgitte stemmene. Ved stemmelikhet avgjør
redaktørens stemme.

----------------------
Redaksjonsrådets møter
----------------------

Redaktøren skal sørge for at redaksjonsrådet holder møte så ofte som det trengs.
Et rådsmedlem, redaksjonssjef eller styret kan kreve at redaksjonsrådet
sammenkalles.

Redaksjonsrådet skal føre protokoll fra rådsmøtene.

Styreleder og daglig leder har møte- og talerett på rådsmøtene, men ikke stemmerett.

---------------------
Ta imot nye medlemmer
---------------------

Redaksjonsrådet skal:

* Ta imot nye medlemmer og finne faddere eller samarbeidspartnere.
* Hjelpe nye medlemmer å finne en passende redaksjon.
* Høre på innslag / program og gi tilbakemeldinger.
* Være tilgjengelig for spørsmål.

----------------
Intern skolering
----------------

Redaksjonsrådet skal:

* være ansvarlig for den interne opplæringen av medlemmene
* holde i gang et faglig forum som skal gi medlemmene en faglig, kreativ og inspirerende utvikling
* arrangere workshops
* trekke inn foredragsholdere utenfra til å forelese over passende emner.


==================
Ansvarlig redaktør
==================

----------------
Valg av redaktør
----------------

Ansvarlig redaktør velges på årsmøtet. Kandidater må være medlem av RadiOrakel.

Redaktørens ansvar

Ansvarlig redaktør er radioens øverste leder og plikter å følge vedtektene,
Redaktørplakaten og Vær Varsom-plakaten. Det er redaktørens ansvar å påse at
RadiOrakel følger konsesjonsvilkår og andre regler for lokalradio.

Redaktøren står ansvarlig for innhold som kringkastes på RadiOrakel og publiseres på radioens
nettsider.


--------------------
Redaktørens oppgaver
--------------------

Redaktøren skal

#. være en del av Redaksjonsrådet
#. lede RadiOrakel sammen med styreleder og redaksjonssjef
#. i fellesskap med styreleder eller daglig leder ha forpliktende signatur for RadiOrakel, jf punkt 6.2 og 6.5.
#. følge opp redaksjonelle vedtak fra redaksjonsrådet
#. på eget initiativ, eller etter forespørsel fra redaksjon / redaksjonsråd, vurdere om programmer og innslag holder tilfredsstillende nivå
#. stoppe programmer / innslag som strider mot radioens formålsparagraf eller bryter med Vær Varsom-plakaten


===========
Opphavsrett
===========

RadiOrakel har opphavsrett til alle programmer som produseres av radioens
medlemmer og sendes i RadiOrakels sendetid. RadiOrakel har opphavsrett til navn
på faste programposter. Dersom medlemmer ønsker å flytte en programpost til en
annen radiostasjon og beholde navnet, må dette skje etter avtale. Slike spørsmål
avgjøres av redaksjonsrådet. Salg / bruk av programmer i andre sammenhenger må
skje etter avtale med redaktøren og daglig leder.

=======
Teknisk
=======

Tekniker:

* Plikter å følge turnus og retningslinjer gitt av teknisk sjef
* Har anledning til å stoppe programmer og innslag på grunn av dårlig teknisk kvalitet
* Skal holde seg oppdatert på det tekniske utstyret
* Skal sørge for erstatningstekniker for egne vakter en er forhindret fra å ta


========
Årsmøtet
========

Årsmøtet er øverste myndighet i RadiOrakel og skal holdes hvert år innen
utgangen av april.

-----------------------------------
Varsel om og innkalling til årsmøte
-----------------------------------

Senest 6 uker før ordinært årsmøte skal styret varsle medlemmene om dato for
møtet og frist for innlevering av saker som ønskes behandlet.

Årsmøtet innkalles skriftlig av styret med minst 14 dagers varsel. Innkallingen
skal inneholde agenda, årsberetning, regnskap og budsjett. Sakspapirene, med
styrets innstilling til vedtak, skal følge innkallingen.

--------------------------------------------
saker som skal behandles på ordinært årsmøte
--------------------------------------------

* Godkjenning av styrets årsberetning og årsregnskap
* Valg av redaktør
* Valg av redaksjonsråd: redaksjonssjef, 3 medlemmer og 2 varamedlemmer
* Valg av styre: styreleder, 4 styremedlemmer og 2 varamedlemmer
* Andre saker nevnt i innkallingen


----------------------
Ekstraordinært årsmøte
----------------------

Ekstraordinært årsmøte holdes når styret finner det nødvendig, eller når minst
10 medlemmer krever det og samtidig oppgir hvilke saker de ønsker behandlet.

Ekstraordinært årsmøte innkalles skriftlig av styret med minst 14 dagers varsel.
Innkallingen skal inneholde agenda og sakspapirer med styrets innstilling til
vedtak.

Dersom man ønsker å fremme mistillitsvotum og foreta nyvalg må det, med minst 14
dagers varsel, innkalles til et forberedende medlemsmøte som oppnevner
valgkomité.

-------------------------------------------
Vedtak på årsmøte og ekstraordinært årsmøte
-------------------------------------------

Årsmøtet er vedtaksført når minst en tredjedel av medlemmene er til stede.

Hvert medlem har én stemme. Det er ikke anledning til å stille med fullmakt fra andre
medlemmer.

Vedtak fattes med mer enn halvparten av de avgitte stemmer. Vedtektsendringer og
mistillitsvotum krever :math:`\frac{2}{3}` flertall.

Ved valg kan årsmøtet på forhånd fastsette at den som får flest stemmer skal
regnes som valgt. Stemmelikhet avgjøres ved loddtrekning.

==========================
Habilitet og taushetsplikt
==========================

Tillitsvalgte i RadiOrakel kan ikke ha økonomiske interesser eller tillitsverv i andre
radiostasjoner. De kan heller ikke delta i virksomhet eller organisasjon som strider mot
RadiOrakels formålsparagraf.

Tillitsvalgte og ansatte har taushetsplikt om personlige forhold de får
kjennskap til gjennom sitt virke i RadiOrakel.


=========================
Vedtektsendringer, opphør
=========================

Vedtektene kan endres med :math:`\frac{2}{3}` flertall på to etterfølgende
årsmøter med minst 2 måneders mellomrom, hvorav minst ett må være ordinært
årsmøte.

Eventuelt opphør av RadiOrakel kan vedtas på samme måte som vedtektsendringer,
men innkalling til det første møtet må ha minimum 6 ukers varsel.
