SPHINXBUILD = sphinx-build
SPHINXPROJ = rakeldok
SOURCEDIR = source
BUILDDIR = build

%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
